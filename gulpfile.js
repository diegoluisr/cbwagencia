'use strict';

const gulp = require('gulp');

gulp.task('clean', (callback) => {
  callback();
});

gulp.task('build:html', () => {
  return gulp.src(['source/index.html'])
});

gulp.task('default', gulp.series('clean', 'build:html'));